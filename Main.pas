unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ComCtrls, Vcl.Mask, untPeFile, Vcl.Grids, Vcl.ImgList;
const
  cDosH : array[0..18] of PAnsiChar = (
                      'e_magic','e_cblp','e_cp','e_crlc','e_cparhdr','e_minalloc',
                      'e_maxalloc','e_ss','e_sp','e_csum','e_ip','e_cs','e_lfarlc',
                      'e_ovno','e_res','e_oemid','e_oeminfo','e_res2','_lfanew');

  cOptH : array[0..29] of PAnsiChar = (
                      'Magic','MajorLinkerVersion','MinorLinkerVersion','SizeOfCode',
                      'SizeOfInitializedData','SizeOfUninitializedData','AddressOfEntryPoint',
                      'BaseOfCode','BaseOfData','ImageBase','SectionAlignment','FileAlignment',
                      'MajorOperatingSystemVersion','MinorOperatingSystemVersion','MajorImageVersion',
                      'MinorImageVersion','MajorSubsystemVersion','MinorSubsystemVersion',
                      'Win32VersionValue','SizeOfImage','SizeOfHeaders','CheckSum','Subsystem',
                      'DllCharacteristics','SizeOfStackReserve','SizeOfStackCommit',
                      'SizeOfHeapReserve','SizeOfHeapCommit','LoaderFlags','NumberOfRvaAndSizes');

type
  TfrmMain = class(TForm)
    Label1: TLabel;
    ImageList1: TImageList;
    tvPeMain: TTreeView;
    btnProcessa: TBitBtn;
    btnAddSec: TBitBtn;
    btnDelSec: TBitBtn;
    btnDumpSec: TBitBtn;
    btnSave: TBitBtn;
    PageControl1: TPageControl;
    tsDatiPe: TTabSheet;
    tsImport: TTabSheet;
    tsRes: TTabSheet;
    sgDati: TStringGrid;
    sgModuli: TStringGrid;
    sgFunz: TStringGrid;
    lvResources: TListView;
    edtCharacteristics: TEdit;
    edtTimeDateStamp: TEdit;
    edtVersion: TEdit;
    edtNumberOfIdEntries: TEdit;
    edtNumberOfNamedEntries: TEdit;
    lvSubResources: TListView;
    odFilePe: TOpenDialog;
    edtFile: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure tvPeMain1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure sgModuliSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure lvResourcesClick(Sender: TObject);
    procedure btnProcessaClick(Sender: TObject);
    procedure btnAddSecClick(Sender: TObject);
    procedure btnDelSecClick(Sender: TObject);
    procedure btnDumpSecClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
  private
    procedure VisDosHeader;
    procedure VisNtHeader;
    procedure VisFileHeader;
    procedure VisOptHeader;
    procedure AutoSizeCol(Grid: TStringGrid; Column: integer;ExtraSize: Byte = 0);
    procedure VisDataDir;
    procedure VisSecHeader;
    procedure VisImportTable;
    procedure VisImportFunction(nModule: Integer);
    procedure VisExportTable;
    procedure VisResource;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  FpeFile       : TPeFile;
  vImportT      : TImportsArray;
   Res          : TResources;
  InizialOffSet : DWORD;

implementation

{$R *.dfm}

procedure TfrmMain.AutoSizeCol(Grid: TStringGrid; Column: integer;ExtraSize: Byte = 0);
var
  i, W, WMax: integer;
begin
  WMax := 0;
  for i := 0 to (Grid.RowCount - 1) do begin
    W := Grid.Canvas.TextWidth(Grid.Cells[Column, i])+ExtraSize;
    if W > WMax then
      WMax := W;
  end;
  Grid.ColWidths[Column] := WMax + 5;
end;

procedure TfrmMain.btnAddSecClick(Sender: TObject);
begin
     FpeFile.AddSection('test',$1000,nil,0) ;
     FpeFile.SaveToFile(edtFile.Text+'.exe')
end;

procedure TfrmMain.btnDelSecClick(Sender: TObject);
begin
     FpeFile.DeleteSection(FpeFile.NumberOfSections-1) ;
     FpeFile.SaveToFile(edtFile.Text+'.exe')
end;

procedure TfrmMain.btnDumpSecClick(Sender: TObject);
begin
     FpeFile.DumpSection(sgDati.row-2,edtFile.Text+'.bin')
end;

procedure TfrmMain.btnProcessaClick(Sender: TObject);
var
   i         : Integer;
   tvMain    : TTreeNode;
   tvDos     : TTreeNode;

   tvNTH     : TTreeNode;
   tvfFileH  : TTreeNode;
   tvOpHeader: TTreeNode;
   tvDataDir : TTreeNode;
   tvSec     : TTreeNode;
   tvImp     : TTreeNode;
   tvExp     : TTreeNode;
   tvRes     : TTreeNode;
begin
     if odFilePe.Execute then
        edtFile.Text := odFilePe.FileName ;


      if edtFile.Text = '' then  Exit;

      FpeFile := TPeFile.Create;

      FpeFile.LoadFromFile(edtFile.Text);
      if not FpeFile.IsValidPe then
            raise Exception.CreateFmt('Formato file non Valido %s :',[edtFile.Text]);
      //
      tvPeMain.Items.Clear;
      tvMain := tvPeMain.Items.add(nil,edtFile.Text);

      tvDos := tvPeMain.Items.AddChild(tvMain,'Dos Header');

      tvNTH     := tvPeMain.Items.AddChild(tvMain,'Nt Headers');
      tvfFileH  := tvPeMain.Items.AddChild(tvNTH,'File Header');
      tvOpHeader:= tvPeMain.Items.AddChild(tvNTH,'Optional Header');
      tvDataDir := tvPeMain.Items.AddChild(tvOpHeader,'Data Directories');

      tvSec     := tvPeMain.Items.AddChild(tvMain,'Section Headers');
      for i := 0 to tvPeMain.Items.Count -1 do
          tvPeMain.Items[i].Expand(True);

      tvImp     := tvPeMain.Items.AddChild(tvMain,'Import Table');
      tvExp     := tvPeMain.Items.AddChild(tvMain,'Export Table');
      tvRes     := tvPeMain.Items.AddChild(tvMain,'Resource');
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     if Assigned(FpeFile) then FpeFile.Free;

end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
     sgDati.RowCount := 0;
     sgDati.ColCount := 0;
end;

procedure TfrmMain.btnSaveClick(Sender: TObject);
begin
    FpeFile.SaveToFile(edtFile.Text+'.Test')
end;

procedure TfrmMain.sgModuliSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
begin
     if tsImport.Caption =  'Import Table' then  VisImportFunction(ARow-3);
end;

procedure TfrmMain.tvPeMain1Click(Sender: TObject);
var
  strTemp : string;
begin
    tsDatiPe.Caption := ExtractFileName(edtFile.Text) ;
    if FpeFile.Is64Bit then  tsDatiPe.Caption := tsDatiPe.Caption+ '- x64-'
    else                     tsDatiPe.Caption := tsDatiPe.Caption+ '- x86-' ;

    InizialOffSet := FpeFile.GetPE32Data(0,PED_PE_OFFSET,strTemp)+4 ;

    if      tvPeMain.Items[1].Selected  then VisDosHeader
    else if tvPeMain.Items[2].Selected  then VisNtHeader
    else if tvPeMain.Items[3].Selected  then VisFileHeader
    else if tvPeMain.Items[4].Selected  then VisOptHeader
    else if tvPeMain.Items[5].Selected  then VisDataDir
    else if tvPeMain.Items[6].Selected  then VisSecHeader
    else if tvPeMain.Items[7].Selected  then VisImportTable
    else if tvPeMain.Items[8].Selected  then VisExportTable
    else if tvPeMain.Items[9].Selected  then VisResource;

end;

(*******************Dos Header*)
procedure TfrmMain.VisDosHeader;
var
   i,count,idx : Integer;
   bkAddr      : NativeUInt;
begin
      sgDati.ColCount := 0;
      sgDati.RowCount := 0;

      sgDati.RowCount := 32;
      sgDati.ColCount := 4;
      sgDati.FixedCols:= 3;
      sgDati.FixedRows:= 1;
      sgDati.Cells[0,0] := 'Member';
      sgDati.Cells[1,0] := 'Offset';
      sgDati.Cells[2,0] := 'Size';
      sgDati.Cells[3,0] := 'Value';

      count := 0;
      idx   := 0;
      bkAddr := NativeUInt(FpeFile.pDosHeader);
      try
      for i := 0 to 30 do
      begin
           if (i < 15) or ((i > 17) and (i < 21))then
           begin
               sgDati.Cells[0,i+1] := string(cDosH[idx]);
               inc(idx);
           end;
           sgDati.Cells[1,i+1] := IntToHex(count,2);
           if i = 30 then  sgDati.Cells[2,i+1] := 'Dword'
           else            sgDati.Cells[2,i+1] := 'Word';

           if i = 30 then
           begin
               sgDati.Cells[3,i+1] := IntToHex(FpeFile.pDosHeader^.e_magic,8);
               sgDati.Cells[0,i+1] := string(cDosH[idx]);
               break;
           end
           else
               sgDati.Cells[3,i+1] := IntToHex(FpeFile.pDosHeader^.e_magic,2);
           inc(NativeUInt(FpeFile.pDosHeader),2);
           inc(count,2);
      end;
      finally
           NativeUInt(FpeFile.pDosHeader):=  bkAddr;
      end;
      AutoSizeCol(sgDati,0);
end;


(*******************Nt Header*)
procedure TfrmMain.VisNtHeader;
var
  strTemp : string;
begin
      sgDati.ColCount := 0;
      sgDati.RowCount := 0;

      sgDati.RowCount := 2;
      sgDati.ColCount := 4;
      sgDati.FixedCols:= 3;
      sgDati.FixedRows:= 1;
      sgDati.Cells[0,0] := 'Member';
      sgDati.Cells[1,0] := 'Offset';
      sgDati.Cells[2,0] := 'Size';
      sgDati.Cells[3,0] := 'Value';

      sgDati.Cells[0,1] := 'Signature';
      sgDati.Cells[1,1] := IntToHex(FpeFile.GetPE32Data(0,PED_PE_OFFSET,strTemp),8);
      sgDati.Cells[2,1] := 'Dword';
      sgDati.Cells[3,1] := IntToHex(FpeFile.pNTHeaders32^.Signature,8) ;
      AutoSizeCol(sgDati,0);
end;

procedure TfrmMain.VisFileHeader;
var
  strtemp       : string;
begin
      sgDati.ColCount := 0;
      sgDati.RowCount := 0;

      sgDati.RowCount := 8;
      sgDati.ColCount := 4;
      sgDati.FixedCols:= 3;
      sgDati.FixedRows:= 1;
      sgDati.Cells[0,0] := 'Member';
      sgDati.Cells[1,0] := 'Offset';
      sgDati.Cells[2,0] := 'Size';
      sgDati.Cells[3,0] := 'Value';

      sgDati.Cells[0,1] := 'Machine';
      sgDati.Cells[1,1] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,2);
      sgDati.Cells[2,1] := 'word';
      sgDati.Cells[3,1] := IntToHex(FpeFile.pNTHeaders32^.FileHeader.Machine,4) ;

      sgDati.Cells[0,2] := 'NumberOfSections';
      sgDati.Cells[1,2] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,2);
      sgDati.Cells[2,2] := 'Word';
      sgDati.Cells[3,2] := IntToHex(FpeFile.GetPE32Data(0,PED_SECTIONNUMBER,strtemp),4);

      sgDati.Cells[0,3] := 'TimeDateStamp';
      sgDati.Cells[1,3] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,3] := 'Dword';
      sgDati.Cells[3,3] := IntToHex(FpeFile.GetPE32Data(0,PED_TIMEDATESTAMP,strtemp),8) ;

      sgDati.Cells[0,4] := 'PointerToSymbolTable';
      sgDati.Cells[1,4] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,4] := 'Dword';
      sgDati.Cells[3,4] := IntToHex(FpeFile.pNTHeaders32^.FileHeader.PointerToSymbolTable,8);

      sgDati.Cells[0,5] := 'NumberOfSymbols';
      sgDati.Cells[1,5] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,5] := 'Dword';
      sgDati.Cells[3,5] := IntToHex(FpeFile.pNTHeaders32^.FileHeader.NumberOfSymbols,8);

      sgDati.Cells[0,6] := 'SizeOfOptionalHeader';
      sgDati.Cells[1,6] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,2);
      sgDati.Cells[2,6] := 'Word';
      sgDati.Cells[3,6] := IntToHex(FpeFile.GetPE32Data(0,PED_SIZEOFOPTIONALHEADER,strtemp),4) ;

      sgDati.Cells[0,7] := 'Characteristics';
      sgDati.Cells[1,7] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,2);
      sgDati.Cells[2,7] := 'Word';
      sgDati.Cells[3,7] := IntToHex(FpeFile.GetPE32Data(0,PED_CHARACTERISTICS,strtemp),4) ;
      AutoSizeCol(sgDati,0);
end;

procedure TfrmMain.VisOptHeader;
var
   count : Integer;
   strTemp : string;
begin
      sgDati.ColCount := 0;
      sgDati.RowCount := 0;

      if FpeFile.Is32Bit then  sgDati.RowCount := 31
      else                     sgDati.RowCount := 30;
      sgDati.ColCount := 4;
      sgDati.FixedCols:= 3;
      sgDati.FixedRows:= 1;
      sgDati.Cells[0,0] := 'Member';
      sgDati.Cells[1,0] := 'Offset';
      sgDati.Cells[2,0] := 'Size';
      sgDati.Cells[3,0] := 'Value';

      count := 0;
      InizialOffSet     := InizialOffSet+sizeof(TImageFileHeader);

      sgDati.Cells[0,1] := string(cOptH[count]);
      sgDati.Cells[1,1] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,2);
      inc(Count);
      sgDati.Cells[2,1] := 'Word';
      sgDati.Cells[3,1] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.Magic,4) ;

      sgDati.Cells[0,2] := string(cOptH[count]);
      sgDati.Cells[1,2] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,1);
      inc(Count);
      sgDati.Cells[2,2] := 'Byte';
      sgDati.Cells[3,2] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MajorLinkerVersion,2) ;

      sgDati.Cells[0,3] := string(cOptH[count]);
      sgDati.Cells[1,3] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,1);
      inc(Count);
      sgDati.Cells[2,3] := 'Byte';
      sgDati.Cells[3,3] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MinorLinkerVersion,2) ;

      sgDati.Cells[0,4] := string(cOptH[count]);
      sgDati.Cells[1,4] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      inc(Count);
      sgDati.Cells[2,4] := 'Dword';
      sgDati.Cells[3,4] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfCode,8) ;

      sgDati.Cells[0,5] := string(cOptH[count]);
      sgDati.Cells[1,5] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      inc(Count);
      sgDati.Cells[2,5] := 'Dword';
      sgDati.Cells[3,5] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfInitializedData,8) ;

      sgDati.Cells[0,6] := string(cOptH[count]);
      sgDati.Cells[1,6] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      inc(Count);
      sgDati.Cells[2,6] := 'Dword';
      sgDati.Cells[3,6] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfUninitializedData,8) ;

      sgDati.Cells[0,7] := string(cOptH[count]);
      sgDati.Cells[1,7] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      inc(Count);
      sgDati.Cells[2,7] := 'Dword';
      sgDati.Cells[3,7] := IntToHex(FpeFile.GetPE32Data(0,PED_OEP,strTemp),8) ;

      sgDati.Cells[0,8] := string(cOptH[count]);
      sgDati.Cells[1,8] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      inc(Count);
      sgDati.Cells[2,8] := 'Dword';
      sgDati.Cells[3,8] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.BaseOfCode,8) ;

      if FpeFile.Is32Bit then
      begin
          sgDati.Cells[0,9] := string(cOptH[count]);
          sgDati.Cells[1,9] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,9] := 'Dword';
          sgDati.Cells[3,9] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.BaseOfData,8) ;

          sgDati.Cells[0,10] := string(cOptH[count]);
          sgDati.Cells[1,10] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,10] := 'Dword';
          sgDati.Cells[3,10] := IntToHex(FpeFile.GetPE32Data(0,PED_IMAGEBASE,strTemp),8) ;

          sgDati.Cells[0,11] := string(cOptH[count]);
          sgDati.Cells[1,11] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,11] := 'Dword';
          sgDati.Cells[3,11] := IntToHex(FpeFile.GetPE32Data(0,PED_SECTIONALIGNMENT,strTemp),8) ;

          sgDati.Cells[0,12] := string(cOptH[count]);
          sgDati.Cells[1,12] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,12] := 'Dword';
          sgDati.Cells[3,12] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.FileAlignment,8) ;

          sgDati.Cells[0,13] := string(cOptH[count]);
          sgDati.Cells[1,13] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,13] := 'Word';
          sgDati.Cells[3,13] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MajorOperatingSystemVersion,4) ;

          sgDati.Cells[0,14] := string(cOptH[count]);
          sgDati.Cells[1,14] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,14] := 'Word';
          sgDati.Cells[3,14] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MinorOperatingSystemVersion,4) ;

          sgDati.Cells[0,15] := string(cOptH[count]);
          sgDati.Cells[1,15] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,15] := 'Word';
          sgDati.Cells[3,15] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MajorImageVersion,4) ;

          sgDati.Cells[0,16] := string(cOptH[count]);
          sgDati.Cells[1,16] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,16] := 'Word';
          sgDati.Cells[3,16] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MinorImageVersion,4) ;

          sgDati.Cells[0,17] := string(cOptH[count]);
          sgDati.Cells[1,17] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,17] := 'Word';
          sgDati.Cells[3,17] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MajorSubsystemVersion,4) ;

          sgDati.Cells[0,18] := string(cOptH[count]);
          sgDati.Cells[1,18] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,18] := 'Word';
          sgDati.Cells[3,18] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.MinorSubsystemVersion,4) ;

          sgDati.Cells[0,19] := string(cOptH[count]);
          sgDati.Cells[1,19] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,19] := 'Dword';
          sgDati.Cells[3,19] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.Win32VersionValue,8) ;

          sgDati.Cells[0,20] := string(cOptH[count]);
          sgDati.Cells[1,20] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,20] := 'Dword';
          sgDati.Cells[3,20] := IntToHex(FpeFile.GetPE32Data(0,PED_SIZEOFIMAGE,strTemp),8) ;

          sgDati.Cells[0,21] := string(cOptH[count]);
          sgDati.Cells[1,21] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,21] := 'Dword';
          sgDati.Cells[3,21] := IntToHex(FpeFile.GetPE32Data(0,PED_SIZEOFHEADERS,strTemp),8) ;

          sgDati.Cells[0,22] := string(cOptH[count]);
          sgDati.Cells[1,22] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,22] := 'Dword';
          sgDati.Cells[3,22] := IntToHex(FpeFile.GetPE32Data(0,PED_CHECKSUM,strTemp),8) ;

          sgDati.Cells[0,23] := string(cOptH[count]);
          sgDati.Cells[1,23] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,23] := 'Word';
          sgDati.Cells[3,23] := IntToHex(FpeFile.GetPE32Data(0,PED_SUBSYSTEM,strTemp),4) ;

          sgDati.Cells[0,24] := string(cOptH[count]);
          sgDati.Cells[1,24] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,24] := 'Word';
          sgDati.Cells[3,24] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DllCharacteristics,4) ;

          sgDati.Cells[0,25] := string(cOptH[count]);
          sgDati.Cells[1,25] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,25] := 'Dword';
          sgDati.Cells[3,25] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfStackReserve,8) ;

          sgDati.Cells[0,26] := string(cOptH[count]);
          sgDati.Cells[1,26] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,26] := 'Dword';
          sgDati.Cells[3,26] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfStackCommit,8) ;

          sgDati.Cells[0,27] := string(cOptH[count]);
          sgDati.Cells[1,27] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,27] := 'Dword';
          sgDati.Cells[3,27] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfHeapReserve,8) ;

          sgDati.Cells[0,28] := string(cOptH[count]);
          sgDati.Cells[1,28] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,28] := 'Dword';
          sgDati.Cells[3,28] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.SizeOfHeapCommit,8) ;

          sgDati.Cells[0,29] := string(cOptH[count]);
          sgDati.Cells[1,29] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,29] := 'Dword';
          sgDati.Cells[3,29] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.LoaderFlags,8) ;

          sgDati.Cells[0,30] := string(cOptH[count]);
          sgDati.Cells[1,30] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          sgDati.Cells[2,30] := 'Dword';
          sgDati.Cells[3,30] := IntToHex(FpeFile.GetPE32Data(0,PED_NUMBEROFRVAANDSIZES,strTemp),8) ;
      end else
      begin
          inc(Count);

          sgDati.Cells[0,9] := string(cOptH[count]);
          sgDati.Cells[1,9] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,8);
          inc(Count);
          sgDati.Cells[2,9] := 'Qword';
          sgDati.Cells[3,9] := IntToHex(FpeFile.GetPE32Data(0,PED_IMAGEBASE,strTemp),16) ;

          sgDati.Cells[0,10] := string(cOptH[count]);
          sgDati.Cells[1,10] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,10] := 'Dword';
          sgDati.Cells[3,10] := IntToHex(FpeFile.GetPE32Data(0,PED_SECTIONALIGNMENT,strTemp),8) ;

          sgDati.Cells[0,11] := string(cOptH[count]);
          sgDati.Cells[1,11] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,11] := 'Dword';
          sgDati.Cells[3,11] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.FileAlignment,8) ;

          sgDati.Cells[0,12] := string(cOptH[count]);
          sgDati.Cells[1,12] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,12] := 'Word';
          sgDati.Cells[3,12] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MajorOperatingSystemVersion,4) ;

          sgDati.Cells[0,13] := string(cOptH[count]);
          sgDati.Cells[1,13] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,13] := 'Word';
          sgDati.Cells[3,13] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MinorOperatingSystemVersion,4) ;

          sgDati.Cells[0,14] := string(cOptH[count]);
          sgDati.Cells[1,14] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,14] := 'Word';
          sgDati.Cells[3,14] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MajorImageVersion,4) ;

          sgDati.Cells[0,15] := string(cOptH[count]);
          sgDati.Cells[1,15] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,15] := 'Word';
          sgDati.Cells[3,15] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MinorImageVersion,4) ;

          sgDati.Cells[0,16] := string(cOptH[count]);
          sgDati.Cells[1,16] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,16] := 'Word';
          sgDati.Cells[3,16] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MajorSubsystemVersion,4) ;

          sgDati.Cells[0,17] := string(cOptH[count]);
          sgDati.Cells[1,17] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,17] := 'Word';
          sgDati.Cells[3,17] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.MinorSubsystemVersion,4) ;

          sgDati.Cells[0,18] := string(cOptH[count]);
          sgDati.Cells[1,18] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,18] := 'Dword';
          sgDati.Cells[3,18] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.Win32VersionValue,8) ;

          sgDati.Cells[0,19] := string(cOptH[count]);
          sgDati.Cells[1,19] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,19] := 'Dword';
          sgDati.Cells[3,19] := IntToHex(FpeFile.GetPE32Data(0,PED_SIZEOFIMAGE,strTemp),8) ;

          sgDati.Cells[0,20] := string(cOptH[count]);
          sgDati.Cells[1,20] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,20] := 'Dword';
          sgDati.Cells[3,20] := IntToHex(FpeFile.GetPE32Data(0,PED_SIZEOFHEADERS,strTemp),8) ;

          sgDati.Cells[0,21] := string(cOptH[count]);
          sgDati.Cells[1,21] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,21] := 'Dword';
          sgDati.Cells[3,21] := IntToHex(FpeFile.GetPE32Data(0,PED_CHECKSUM,strTemp),8) ;

          sgDati.Cells[0,22] := string(cOptH[count]);
          sgDati.Cells[1,22] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,22] := 'Word';
          sgDati.Cells[3,22] := IntToHex(FpeFile.GetPE32Data(0,PED_SUBSYSTEM,strTemp),4) ;

          sgDati.Cells[0,23] := string(cOptH[count]);
          sgDati.Cells[1,23] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,2);
          inc(Count);
          sgDati.Cells[2,23] := 'Word';
          sgDati.Cells[3,23] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DllCharacteristics,4) ;

          sgDati.Cells[0,24] := string(cOptH[count]);
          sgDati.Cells[1,24] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,8);
          inc(Count);
          sgDati.Cells[2,24] := 'Qword';
          sgDati.Cells[3,24] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.SizeOfStackReserve,16) ;

          sgDati.Cells[0,25] := string(cOptH[count]);
          sgDati.Cells[1,25] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,8);
          inc(Count);
          sgDati.Cells[2,25] := 'Qword';
          sgDati.Cells[3,25] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.SizeOfStackCommit,16) ;

          sgDati.Cells[0,26] := string(cOptH[count]);
          sgDati.Cells[1,26] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,8);
          inc(Count);
          sgDati.Cells[2,26] := 'Qword';
          sgDati.Cells[3,26] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.SizeOfHeapReserve,16) ;

          sgDati.Cells[0,27] := string(cOptH[count]);
          sgDati.Cells[1,27] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,8);
          inc(Count);
          sgDati.Cells[2,27] := 'Qword';
          sgDati.Cells[3,27] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.SizeOfHeapCommit,16) ;

          sgDati.Cells[0,28] := string(cOptH[count]);
          sgDati.Cells[1,28] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          inc(Count);
          sgDati.Cells[2,28] := 'Dword';
          sgDati.Cells[3,28] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.LoaderFlags,8) ;

          sgDati.Cells[0,29] := string(cOptH[count]);
          sgDati.Cells[1,29] := IntToHex(InizialOffSet,4);
          inc(InizialOffSet,4);
          sgDati.Cells[2,29] := 'Dword';
          sgDati.Cells[3,29] := IntToHex(FpeFile.GetPE32Data(0,PED_NUMBEROFRVAANDSIZES,strTemp),8) ;
      end;



      AutoSizeCol(sgDati,0);
      AutoSizeCol(sgDati,3);

end;

procedure TfrmMain.VisDataDir;
var
   strTemp     : string;
begin

      sgDati.ColCount := 0;
      sgDati.RowCount := 0;

      sgDati.RowCount := 31;

      sgDati.ColCount := 4;
      sgDati.FixedCols:= 3;
      sgDati.FixedRows:= 1;
      sgDati.Cells[0,0] := 'Member';
      sgDati.Cells[1,0] := 'Offset';
      sgDati.Cells[2,0] := 'Size';
      sgDati.Cells[3,0] := 'Value';

      if FpeFile.Is32Bit then
             InizialOffSet     := InizialOffSet+(sizeof(TImageFileHeader)+sizeof(TImageOptionalHeader32) -SizeOf(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory))
      else
             InizialOffSet     := InizialOffSet+(sizeof(TImageFileHeader)+sizeof(TImageOptionalHeader64)-SizeOf(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory));

      sgDati.Cells[0,1] := 'Export Directory RVA';
      sgDati.Cells[1,1] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,1] := 'Dword';
      sgDati.Cells[3,1] := IntToHex(FpeFile.GetPE32Data(0,PED_EXPORTTABLEADDRESS,strTemp),8) ;

      sgDati.Cells[0,2] := 'Export Directory Size';
      sgDati.Cells[1,2] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,2] := 'Dword';
      sgDati.Cells[3,2] := IntToHex(FpeFile.GetPE32Data(0,PED_EXPORTTABLESIZE,strTemp),8) ;

      sgDati.Cells[0,3] := 'Import Directory RVA';
      sgDati.Cells[1,3] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,3] := 'Dword';
      sgDati.Cells[3,3] := IntToHex(FpeFile.GetPE32Data(0,PED_IMPORTTABLEADDRESS,strTemp),8) ;

      sgDati.Cells[0,4] := 'Import Directory Size';
      sgDati.Cells[1,4] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,4] := 'Dword';
      sgDati.Cells[3,4] := IntToHex(FpeFile.GetPE32Data(0,PED_IMPORTTABLESIZE,strTemp),8) ;

      sgDati.Cells[0,5] := 'Resource Directory RVA';
      sgDati.Cells[1,5] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,5] := 'Dword';
      sgDati.Cells[3,5] := IntToHex(FpeFile.GetPE32Data(0,PED_RESOURCETABLEADDRESS,strTemp),8) ;

      sgDati.Cells[0,6] := 'Resource Directory Size';
      sgDati.Cells[1,6] := IntToHex(InizialOffSet,4);
      inc(InizialOffSet,4);
      sgDati.Cells[2,6] := 'Dword';
      sgDati.Cells[3,6] := IntToHex(FpeFile.GetPE32Data(0,PED_RESOURCETABLESIZE,strTemp),8) ;

      if FpeFile.Is32Bit then
      begin
           sgDati.Cells[0,7] := 'Exception Directory RVA';
           sgDati.Cells[1,7] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,7] := 'Dword';
           sgDati.Cells[3,7] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].VirtualAddress,8) ;

           sgDati.Cells[0,8] := 'Exception Directory Size';
           sgDati.Cells[1,8] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,8] := 'Dword';
           sgDati.Cells[3,8] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].Size,8) ;

           sgDati.Cells[0,9] := 'Security Directory RVA';
           sgDati.Cells[1,9] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,9] := 'Dword';
           sgDati.Cells[3,9] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORy_ENTRY_SECURITY].VirtualAddress,8) ;

           sgDati.Cells[0,10] := 'Security Directory Size';
           sgDati.Cells[1,10] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,10] := 'Dword';
           sgDati.Cells[3,10] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_SECURITY].Size,8) ;

           sgDati.Cells[0,11] := 'Relocation Directory RVA';
           sgDati.Cells[1,11] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,11] := 'Dword';
           sgDati.Cells[3,11] := IntToHex(FpeFile.GetPE32Data(0,PED_RELOCATIONTABLEADDRESS,strTemp),8) ;

           sgDati.Cells[0,12] := 'Relocation Directory Size';
           sgDati.Cells[1,12] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,12] := 'Dword';
           sgDati.Cells[3,12] := IntToHex(FpeFile.GetPE32Data(0,PED_RELOCATIONTABLESIZE,strTemp),8) ;

           sgDati.Cells[0,13] := 'Debug Directory RVA';
           sgDati.Cells[1,13] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,13] := 'Dword';
           sgDati.Cells[3,13] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress,8) ;

           sgDati.Cells[0,14] := 'Debug Directory Size';
           sgDati.Cells[1,14] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,14] := 'Dword';
           sgDati.Cells[3,14] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size,8) ;

           sgDati.Cells[0,15] := 'Architecture Directory RVA';
           sgDati.Cells[1,15] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,15] := 'Dword';
           sgDati.Cells[3,15] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COPYRIGHT].VirtualAddress,8) ;

           sgDati.Cells[0,16] := 'Architecture Directory Size';
           sgDati.Cells[1,16] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,16] := 'Dword';
           sgDati.Cells[3,16] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COPYRIGHT].Size,8) ;

           sgDati.Cells[0,17] := 'Reserved Directory RVA';
           sgDati.Cells[1,17] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,17] := 'Dword';
           sgDati.Cells[3,17] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_GLOBALPTR].VirtualAddress,8) ;

           sgDati.Cells[0,18] := 'Reserved Directory Size';
           sgDati.Cells[1,18] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,18] := 'Dword';
           sgDati.Cells[3,18] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_GLOBALPTR].Size,8) ;

           sgDati.Cells[0,19] := 'TLS Directory RVA';
           sgDati.Cells[1,19] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,19] := 'Dword';
           sgDati.Cells[3,19] := IntToHex(FpeFile.GetPE32Data(0,PED_TLSTABLEADDRESS,strTemp),8) ;

           sgDati.Cells[0,20] := 'TLS Directory Size';
           sgDati.Cells[1,20] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,20] := 'Dword';
           sgDati.Cells[3,20] := IntToHex(FpeFile.GetPE32Data(0,PED_TLSTABLESIZE,strTemp),8) ;

           sgDati.Cells[0,21] := 'Configuration Directory RVA';
           sgDati.Cells[1,21] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,21] := 'Dword';
           sgDati.Cells[3,21] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].VirtualAddress,8) ;

           sgDati.Cells[0,22] := 'Configuration Directory Size';
           sgDati.Cells[1,22] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,22] := 'Dword';
           sgDati.Cells[3,22] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].Size,8) ;

           sgDati.Cells[0,23] := 'Bound Import Directory RVA';
           sgDati.Cells[1,23] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,23] := 'Dword';
           sgDati.Cells[3,23] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT].VirtualAddress,8) ;

           sgDati.Cells[0,24] := 'Bound Import Directory Size';
           sgDati.Cells[1,24] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,24] := 'Dword';
           sgDati.Cells[3,24] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT].Size,8) ;

           sgDati.Cells[0,25] := 'Import Address Table Directory RVA';
           sgDati.Cells[1,25] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,25] := 'Dword';
           sgDati.Cells[3,25] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].VirtualAddress,8) ;

           sgDati.Cells[0,26] := 'Import Address Table Directory Size';
           sgDati.Cells[1,26] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,26] := 'Dword';
           sgDati.Cells[3,26] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].Size,8) ;

           sgDati.Cells[0,27] := 'Deley Import Directory RVA';
           sgDati.Cells[1,27] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,27] := 'Dword';
           sgDati.Cells[3,27] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT].VirtualAddress,8) ;

           sgDati.Cells[0,28] := 'Deley Import Directory Size';
           sgDati.Cells[1,28] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,28] := 'Dword';
           sgDati.Cells[3,28] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT].Size,8) ;

           sgDati.Cells[0,29] := '.NET Metadata Directory RVA';
           sgDati.Cells[1,29] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,29] := 'Dword';
           sgDati.Cells[3,29] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].VirtualAddress,8) ;

           sgDati.Cells[0,30] := 'NET Metadata Directory Size';
           sgDati.Cells[1,30] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,30] := 'Dword';
           sgDati.Cells[3,30] := IntToHex(FpeFile.pNTHeaders32^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].Size,8) ;
      end else
      begin
           sgDati.Cells[0,7] := 'Exception Directory RVA';
           sgDati.Cells[1,7] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,7] := 'Dword';
           sgDati.Cells[3,7] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].VirtualAddress,8) ;

           sgDati.Cells[0,8] := 'Exception Directory Size';
           sgDati.Cells[1,8] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,8] := 'Dword';
           sgDati.Cells[3,8] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXCEPTION].Size,8) ;

           sgDati.Cells[0,9] := 'Security Directory RVA';
           sgDati.Cells[1,9] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,9] := 'Dword';
           sgDati.Cells[3,9] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_SECURITY].VirtualAddress,8) ;

           sgDati.Cells[0,10] := 'Security Directory Size';
           sgDati.Cells[1,10] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,10] := 'Dword';
           sgDati.Cells[3,10] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_SECURITY].Size,8) ;

           sgDati.Cells[0,11] := 'Relocation Directory RVA';
           sgDati.Cells[1,11] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,11] := 'Dword';
           sgDati.Cells[3,11] := IntToHex(FpeFile.GetPE32Data(0,PED_RELOCATIONTABLEADDRESS,strTemp),8) ;

           sgDati.Cells[0,12] := 'Relocation Directory Size';
           sgDati.Cells[1,12] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,12] := 'Dword';
           sgDati.Cells[3,12] := IntToHex(FpeFile.GetPE32Data(0,PED_RELOCATIONTABLESIZE,strTemp),8) ;

           sgDati.Cells[0,13] := 'Debug Directory RVA';
           sgDati.Cells[1,13] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,13] := 'Dword';
           sgDati.Cells[3,13] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress,8) ;

           sgDati.Cells[0,14] := 'Debug Directory Size';
           sgDati.Cells[1,14] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,14] := 'Dword';
           sgDati.Cells[3,14] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size,8) ;

           sgDati.Cells[0,15] := 'Architecture Directory RVA';
           sgDati.Cells[1,15] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,15] := 'Dword';
           sgDati.Cells[3,15] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COPYRIGHT].VirtualAddress,8) ;

           sgDati.Cells[0,16] := 'Architecture Directory Size';
           sgDati.Cells[1,16] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,16] := 'Dword';
           sgDati.Cells[3,16] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COPYRIGHT].Size,8) ;

           sgDati.Cells[0,17] := 'Reserved Directory RVA';
           sgDati.Cells[1,17] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,17] := 'Dword';
           sgDati.Cells[3,17] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_GLOBALPTR].VirtualAddress,8) ;

           sgDati.Cells[0,18] := 'Reserved Directory Size';
           sgDati.Cells[1,18] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,18] := 'Dword';
           sgDati.Cells[3,18] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_GLOBALPTR].Size,8) ;

           sgDati.Cells[0,19] := 'TLS Directory RVA';
           sgDati.Cells[1,19] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,19] := 'Dword';
           sgDati.Cells[3,19] := IntToHex(FpeFile.GetPE32Data(0,PED_TLSTABLEADDRESS,strTemp),8) ;

           sgDati.Cells[0,20] := 'TLS Directory Size';
           sgDati.Cells[1,20] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,20] := 'Dword';
           sgDati.Cells[3,20] := IntToHex(FpeFile.GetPE32Data(0,PED_TLSTABLESIZE,strTemp),8) ;

           sgDati.Cells[0,21] := 'Configuration Directory RVA';
           sgDati.Cells[1,21] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,21] := 'Dword';
           sgDati.Cells[3,21] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].VirtualAddress,8) ;

           sgDati.Cells[0,22] := 'Configuration Directory Size';
           sgDati.Cells[1,22] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,22] := 'Dword';
           sgDati.Cells[3,22] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG].Size,8) ;

           sgDati.Cells[0,23] := 'Bound Import Directory RVA';
           sgDati.Cells[1,23] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,23] := 'Dword';
           sgDati.Cells[3,23] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT].VirtualAddress,8) ;

           sgDati.Cells[0,24] := 'Bound Import Directory Size';
           sgDati.Cells[1,24] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,24] := 'Dword';
           sgDati.Cells[3,24] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT].Size,8) ;

           sgDati.Cells[0,25] := 'Import Address Table Directory RVA';
           sgDati.Cells[1,25] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,25] := 'Dword';
           sgDati.Cells[3,25] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].VirtualAddress,8) ;

           sgDati.Cells[0,26] := 'Import Address Table Directory Size';
           sgDati.Cells[1,26] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,26] := 'Dword';
           sgDati.Cells[3,26] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IAT].Size,8) ;

           sgDati.Cells[0,27] := 'Deley Import Directory RVA';
           sgDati.Cells[1,27] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,27] := 'Dword';
           sgDati.Cells[3,27] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT].VirtualAddress,8) ;

           sgDati.Cells[0,28] := 'Deley Import Directory Size';
           sgDati.Cells[1,28] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,28] := 'Dword';
           sgDati.Cells[3,28] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT].Size,8) ;

           sgDati.Cells[0,29] := '.NET Metadata Directory RVA';
           sgDati.Cells[1,29] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,29] := 'Dword';
           sgDati.Cells[3,29] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].VirtualAddress,8) ;

           sgDati.Cells[0,30] := 'NET Metadata Directory Size';
           sgDati.Cells[1,30] := IntToHex(InizialOffSet,4);
           inc(InizialOffSet,4);
           sgDati.Cells[2,30] := 'Dword';
           sgDati.Cells[3,30] := IntToHex(FpeFile.pNTHeaders64^.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR].Size,8) ;
      end;
      AutoSizeCol(sgDati,0);
      AutoSizeCol(sgDati,3);
end;

(*******************Section Header*)
procedure TfrmMain.VisSecHeader;
var
  i : Integer;
  strTemp : string;
begin
     sgDati.ColCount := 0;
     sgDati.RowCount := 0;

     sgDati.RowCount := FpeFile.GetPE32Data(0,PED_SECTIONNUMBER,strTemp)+2;

     sgDati.ColCount := 10;
     sgDati.FixedCols:= 0;
     sgDati.FixedRows:= 2;
     sgDati.Cells[0,0] := 'Name';
     sgDati.Cells[1,0] := 'Virtual Size';
     sgDati.Cells[2,0] := 'Virtual Address';
     sgDati.Cells[3,0] := 'Raw Size';
     sgDati.Cells[4,0] := 'Raw Address';
     sgDati.Cells[5,0] := 'Reloc Address';
     sgDati.Cells[6,0] := 'Linenumbers';
     sgDati.Cells[7,0] := 'Relocation Number';
     sgDati.Cells[8,0] := 'Linenumbers Number';
     sgDati.Cells[9,0] := 'Characteristics';

     sgDati.Cells[0,1] := 'Byte[8]';
     sgDati.Cells[1,1] := 'Dword';
     sgDati.Cells[2,1] := 'Dword';
     sgDati.Cells[3,1] := 'Dword';
     sgDati.Cells[4,1] := 'Dword';
     sgDati.Cells[5,1] := 'Dword';
     sgDati.Cells[6,1] := 'Dword';
     sgDati.Cells[7,1] := 'Word';
     sgDati.Cells[8,1] := 'Word';
     sgDati.Cells[9,1] := 'Dword';

     for i := 0 to High(FpeFile.ImageSections) do
     begin
          FpeFile.GetPE32Data(i,PED_SECTIONNAME,strTemp);
          sgDati.Cells[0,i+2] := strTemp;
          sgDati.Cells[1,i+2] := IntToHex(FpeFile.GetPE32Data(i,PED_SECTIONVIRTUALSIZE,strTemp),8);
          sgDati.Cells[2,i+2] := IntToHex(FpeFile.GetPE32Data(i,PED_SECTIONVIRTUALOFFSET,strTemp),8);
          sgDati.Cells[3,i+2] := IntToHex(FpeFile.GetPE32Data(i,PED_SECTIONRAWSIZE,strTemp),8);
          sgDati.Cells[4,i+2] := IntToHex(FpeFile.GetPE32Data(i,PED_SECTIONRAWOFFSET,strTemp),8);
          sgDati.Cells[5,i+2] := IntToHex(FpeFile.ImageSections[i].PointerToRelocations,8);
          sgDati.Cells[6,i+2] := IntToHex(FpeFile.ImageSections[i].PointerToLinenumbers,8);
          sgDati.Cells[7,i+2] := IntToHex(FpeFile.ImageSections[i].NumberOfRelocations,4);
          sgDati.Cells[8,i+2] := IntToHex(FpeFile.ImageSections[i].NumberOfLinenumbers,4);
          sgDati.Cells[9,i+2] := IntToHex(FpeFile.GetPE32Data(i,PED_SECTIONFLAGS,strTemp),8);
     end;

end;

procedure TfrmMain.VisImportTable;
var
   i : Integer;
begin
      tsImport.Caption := 'Import Table';

      FpeFile.GetImportAddressTable( vImportT );

      if Length(vImportT) = 0 then  Exit;

      for i := 0 to sgModuli.ColCount - 1 do
          sgModuli.Cols[i].Clear;
      for i := 0 to sgFunz.ColCount - 1 do
          sgFunz.Cols[i].Clear;

      sgModuli.ColCount := 0;
      sgModuli.RowCount := 0;

      sgModuli.RowCount := Length(vImportT)+3;
      sgModuli.FixedRows:= 3;
      sgModuli.ColCount := 7;
      sgModuli.FixedCols:= 0;

      sgModuli.Cells[0,0] := 'Module Name';
      sgModuli.Cells[1,0] := 'Import';
      sgModuli.Cells[2,0] := 'OFTk';
      sgModuli.Cells[3,0] := 'TimeDataStamp';
      sgModuli.Cells[4,0] := 'ForwarderChain';
      sgModuli.Cells[5,0] := '  Name RVA  ';
      sgModuli.Cells[6,0] := '  FTs(IAT)  ';

      sgModuli.Cells[0,2] := 'szAnsi';
      sgModuli.Cells[1,2] := '(nFunction)';
      sgModuli.Cells[2,2] := 'Dword';
      sgModuli.Cells[3,2] := 'Dword';
      sgModuli.Cells[4,2] := 'Dword';
      sgModuli.Cells[5,2] := 'Dword';
      sgModuli.Cells[6,2] := 'Dword';

      for i := 0 to High(vImportT) do
      begin
           sgModuli.Cells[0,i+3] := vImportT[i].LibraryName;
           sgModuli.Cells[1,i+3] := IntToHex(Length(vImportT[i].IatFunctions),4);
           sgModuli.Cells[2,i+3] := IntToHex(vImportT[i].OriginalFirstThunk,8);
           sgModuli.Cells[3,i+3] := IntToHex(vImportT[i].TimeDateStamp,8);
           sgModuli.Cells[4,i+3] := IntToHex(vImportT[i].ForwarderChain,8);
           sgModuli.Cells[5,i+3] := IntToHex(vImportT[i].Name,8);
           sgModuli.Cells[6,i+3] := IntToHex(vImportT[i].FirstThunk,8);
      end;

      for i := 0 to sgModuli.ColCount -1 do
          AutoSizeCol(sgModuli,i,10);

      VisImportFunction(0);

end;

procedure TfrmMain.VisImportFunction(nModule: Integer);
var
   i : Integer;
begin
      tsImport.Caption := 'Import Table';

      for i := 0 to sgFunz.ColCount - 1 do
          sgFunz.Cols[i].Clear;

      sgFunz.ColCount := 0;
      sgFunz.RowCount := 0;

      sgFunz.RowCount := Length(vImportT[nModule].IatFunctions)+3;
      sgFunz.FixedRows:= 3;
      sgFunz.ColCount := 5;
      sgFunz.FixedCols:= 0;

      sgFunz.Cells[0,0] := 'ThunkRVA';
      sgFunz.Cells[1,0] := 'ThunkOffset';
      sgFunz.Cells[2,0] := 'FTs(IAT)';
      sgFunz.Cells[3,0] := 'Hint';
      sgFunz.Cells[4,0] := 'Name';

      sgFunz.Cells[0,2] := 'Dword';
      sgFunz.Cells[1,2] := 'Dword';
      sgFunz.Cells[2,2] := 'Dword';
      sgFunz.Cells[3,2] := 'Word';
      sgFunz.Cells[4,2] := 'szAnsi';

      if Length(vImportT[nModule].IatFunctions) = 0 then Exit;

      if FpeFile.Is32Bit then
      begin
          for i := 0 to High(vImportT[nModule].IatFunctions) do
          begin
               sgFunz.Cells[0,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkRVA,8);
               sgFunz.Cells[1,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkOffset,8);
               sgFunz.Cells[2,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkValue,8);
               sgFunz.Cells[3,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].Hint,4);
               sgFunz.Cells[4,i+3] := vImportT[nModule].IatFunctions[i].ApiName;

          end;
      end else
      begin
          sgFunz.Cells[2,2] := 'QDword';

          for i := 0 to High(vImportT[nModule].IatFunctions) do
          begin
               sgFunz.Cells[0,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkRVA,8);
               sgFunz.Cells[1,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkOffset,8);
               sgFunz.Cells[2,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].ThunkValue,16);
               sgFunz.Cells[3,i+3] := IntToHex(vImportT[nModule].IatFunctions[i].Hint,4);
               sgFunz.Cells[4,i+3] := vImportT[nModule].IatFunctions[i].ApiName;

          end;
      end;

      for i := 0 to sgFunz.ColCount -1 do
          AutoSizeCol(sgFunz,i,10);
end;

procedure TfrmMain.VisExportTable;
var
   i           : Integer;
   vExportT    : TExports;
   ExportOffs  : Cardinal;
begin
     tsImport.Caption := 'Export Table';

      ExportOffs := FpeFile.GetExportsAddressTable( vExportT );
      if vExportT.NumberOfFunctions = 0 then Exit;

      for i := 0 to sgModuli.ColCount - 1 do
          sgModuli.Cols[i].Clear;
      for i := 0 to sgFunz.ColCount - 1 do
          sgFunz.Cols[i].Clear;

      sgModuli.ColCount := 0;
      sgModuli.RowCount := 0;

      sgModuli.ColCount := 4;
      sgModuli.FixedCols:= 3;
      sgModuli.RowCount := 12;
      sgModuli.FixedRows:= 1;

      sgModuli.Cells[0,0] := 'Member';
      sgModuli.Cells[1,0] := 'Offset';
      sgModuli.Cells[2,0] := 'Size';
      sgModuli.Cells[3,0] := 'Value';

      sgModuli.Cells[0,1] := 'Name';
      sgModuli.Cells[0,2] := 'Base';
      sgModuli.Cells[0,3] := 'Characteristics';
      sgModuli.Cells[0,4] := 'TimeDateStamp';
      sgModuli.Cells[0,5] := 'MajorVersion';
      sgModuli.Cells[0,6] := 'MinorVersion';
      sgModuli.Cells[0,7] := 'NumberOfFunctions';
      sgModuli.Cells[0,8] := 'NumberOfNames';
      sgModuli.Cells[0,9] := 'AddressOfFunctions';
      sgModuli.Cells[0,10]:= 'AddressOfNames';
      sgModuli.Cells[0,11]:= 'AddressOfNameOrdinals';

      sgModuli.Cells[3,1] := vExportT.LibraryName;
      sgModuli.Cells[3,2] := IntToHex(vExportT.Base,8);
      sgModuli.Cells[3,3] := IntToHex(vExportT.Characteristics,8);
      sgModuli.Cells[3,4] := IntToHex(vExportT.TimeDateStamp,8);
      sgModuli.Cells[3,5] := IntToHex(vExportT.MajorVersion,4);
      sgModuli.Cells[3,6] := IntToHex(vExportT.MinorVersion,4);
      sgModuli.Cells[3,7] := IntToHex(vExportT.NumberOfFunctions,8);
      sgModuli.Cells[3,8] := IntToHex(vExportT.NumberOfNames,8);
      sgModuli.Cells[3,9] := IntToHex(vExportT.AddressOfFunctions,8);
      sgModuli.Cells[3,10]:= IntToHex(vExportT.AddressOfNames,8);
      sgModuli.Cells[3,11]:= IntToHex(vExportT.AddressOfNameOrdinals,8);

      sgModuli.Cells[2,1] := 'szAnsi';
      sgModuli.Cells[2,2] := 'Dword';
      sgModuli.Cells[2,3] := 'Dword';
      sgModuli.Cells[2,4] := 'Dword';
      sgModuli.Cells[2,5] := 'Word';
      sgModuli.Cells[2,6] := 'Word';
      sgModuli.Cells[2,7] := 'Dword';
      sgModuli.Cells[2,8] := 'Dword';
      sgModuli.Cells[2,9] := 'Dword';
      sgModuli.Cells[2,10]:= 'Dword';
      sgModuli.Cells[2,11]:= 'Dword';

     { sgModuli.Cells[1,1] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,2] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,3] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,4] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,5] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,6] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,7] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,8] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,9] := IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,10]:= IntToHex(vExportT.Characteristics,8);;
      sgModuli.Cells[1,11]:= IntToHex(vExportT.Characteristics,8);;}

      sgFunz.ColCount := 0;
      sgFunz.RowCount := 0;

      sgFunz.ColCount := 5;
      sgFunz.FixedCols:= 0;

      sgFunz.RowCount := Length(vExportT.ExportFunctions)+3;
      sgFunz.FixedRows:= 3;

      sgFunz.Cells[0,0] := 'Ordinal';
      sgFunz.Cells[1,0] := 'Function RVA';
      sgFunz.Cells[2,0] := 'Name Ordinal';
      sgFunz.Cells[3,0] := 'Name RVA';
      sgFunz.Cells[4,0] := 'Name';

      sgFunz.Cells[0,2] := '(nFunctions)';
      sgFunz.Cells[1,2] := 'Dword';
      sgFunz.Cells[2,2] := 'Word';
      sgFunz.Cells[3,2] := 'Dword';
      sgFunz.Cells[4,2] := 'szAnsi';

      for i := 0 to High(vExportT.ExportFunctions) do
      begin
           sgFunz.Cells[4,i+3] := vExportT.ExportFunctions[i].ApiName;
           sgFunz.Cells[0,i+3] := IntToHex(vExportT.ExportFunctions[i].Ordinal,4);
           sgFunz.Cells[3,i+3] := IntToHex(vExportT.ExportFunctions[i].Rva,8);
           sgFunz.Cells[1,i+3] := IntToHex(vExportT.ExportFunctions[i].FileOffset,8);
      end;

      for i := 0 to sgModuli.ColCount -1 do
          AutoSizeCol(sgModuli,i,10);
      for i := 0 to sgFunz.ColCount -1 do
          AutoSizeCol(sgFunz,i,10);
end;

const
   sFormatStr                = '%2.8x';
   sFormatStr2               = '%2.4x';

procedure TfrmMain.VisResource;

var
  x: Integer;
  ListItem: TListItem;
begin
    lvResources.Clear;
    lvSubResources.Clear;
    FpeFile.GetResources(Res);

    edtCharacteristics.Text      := Format(sFormatStr, [Res.Dir.Characteristics]);
    edtTimeDateStamp.Text        := Format(sFormatStr, [Res.Dir.TimeDateStamp]);
    edtVersion.Text              := Format('%d.%d', [Res.Dir.MajorVersion, Res.Dir.MinorVersion]);
    edtNumberOfNamedEntries.Text := Format(sFormatStr2, [Res.Dir.NumberOfNamedEntries]);
    edtNumberOfIdEntries.Text    := Format(sFormatStr2, [Res.Dir.NumberOfIdEntries]);
    for x := Low(Res.Entries) to High(Res.Entries) do
    begin
      ListItem := lvResources.Items.Add;
      ListItem.Caption := Res.Entries[x].sTyp;
      ListItem.ImageIndex := 0;
    end;
end;

procedure TfrmMain.lvResourcesClick(Sender: TObject);
var
  x: Integer;
  ListItem: TListItem;
begin
  lvSubResources.Clear;
  lvSubResources.Items.BeginUpdate;
  if (lvResources.Selected <> nil) then
  begin
    for x := Low(Res.Entries[lvResources.Selected.Index].NameEntries) to High(Res.Entries[lvResources.Selected.Index].NameEntries) do
    begin
      ListItem := lvSubResources.Items.Add;
      ListItem.Caption := Res.Entries[lvResources.Selected.Index].NameEntries[x].sName;
      ListItem.SubItems.Add(Format(sFormatStr, [Res.Entries[lvResources.Selected.Index].NameEntries[x].dwDataRVA]));
      ListItem.SubItems.Add(Format(sFormatStr, [Res.Entries[lvResources.Selected.Index].NameEntries[x].dwsize]));
      ListItem.SubItems.Add(Res.Entries[lvResources.Selected.Index].NameEntries[x].sLang);
    end;
  end;
  lvSubResources.Items.EndUpdate;
end;

end.
